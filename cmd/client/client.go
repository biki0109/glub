package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/glub/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type GlubClient struct {
	pb.GlubClient
}

func NewGlubClient(address string) *GlubClient {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial Glub service", l.Error(err))
	}

	c := pb.NewGlubClient(conn)

	return &GlubClient{c}
}
