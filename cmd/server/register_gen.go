// IM AUTO GENERATED, BUT CAN BE OVERRIDDEN

package main

import (
	"git.begroup.team/platform-transport/glub/internal/services"
	"git.begroup.team/platform-transport/glub/internal/stores"
	"git.begroup.team/platform-transport/glub/pb"
	"honnef.co/go/tools/config"
)

func registerService(cfg *config.Config) pb.GlubServer {

	mainStore := stores.NewMainStore()

	return services.New(cfg, mainStore)
}
